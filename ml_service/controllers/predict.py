# Импорт сторонних библиотек
import pandas as pd
#from config import METRICS

# Импорты модулей текущего проекта
from model import model
from data_types import PredictionIn

# Выдача предсказания
class Predict():
    #@METRICS.timer('code.classes.Balancer.predict')
    @staticmethod
    def predict_features(item: PredictionIn):
        # Конвертация входных данных в датафрейм (для передачи в модель)
        input_data = pd.DataFrame([dict(item)])

        # Выполнение предсказания модели
        prediction = model.predict(input_data)
        prediction_out = prediction[0].round()

        return {'prediction': prediction_out}