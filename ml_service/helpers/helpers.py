# Импорт сторонних библиотек
import pandas as pd
from sklearn.preprocessing import LabelEncoder
from sklearn.base import BaseEstimator, TransformerMixin


# Преобразование даты начала сборки в категориальные признаки
class DateTimeConverter(BaseEstimator, TransformerMixin):
    def __init__(self, column_name, timezone_offset):
        self.column_name = column_name
        self.timezone_offset = timezone_offset

    def fit(self, X, y=None):
        return self

    def transform(self, X):
        x_copy = X.copy()
        x_copy[self.column_name] = pd.to_datetime(x_copy[self.column_name])
        x_copy[self.timezone_offset] = pd.to_timedelta(x_copy[self.timezone_offset], unit='m')
        x_copy[self.column_name] += x_copy[self.timezone_offset]

        x_copy['hour_of_day'] = x_copy[self.column_name].dt.strftime('%H')
        x_copy['weekday_num'] = x_copy[self.column_name].dt.weekday
        
        x_copy = x_copy.drop(labels=[self.column_name, self.timezone_offset], axis=1)
        print("datetime converted")

        return x_copy


# Обработка неизвестных категорий в признаке region
class UnknownCategoryImputer(BaseEstimator, TransformerMixin):
    def __init__(self, column, default_value):
        self.column = column
        self.default_value = default_value

    def fit(self, X, y=None):
        self.known_categories_ = set(X[self.column])
        return self

    def transform(self, X, y=None):
        unknown_categories_mask = ~X[self.column].isin(self.known_categories_)
        unknown_categories_count = unknown_categories_mask.sum()

        print("Unknown regions count:", unknown_categories_count)

        X.loc[unknown_categories_mask, self.column] = self.default_value
        return X


# Кодирование категориальных признаков с помощью Label Encoding
class MultiColumnLabelEncoder():
    def __init__(self, columns=None):
        self.columns = columns

    def fit(self, X, y=None):
        self.encoders = {}
        columns = X.columns if self.columns is None else self.columns
        for col in columns:
            self.encoders[col] = LabelEncoder().fit(X[col])
        return self

    def transform(self, X):
        output = X.copy()
        for col, encoder in self.encoders.items():
            output[col] = encoder.transform(output[col])

        print("Features Encoded")
        
        return output

    # для обратного преобразованиям (если понадобится)
    def inverse_transform(self, X):
        output = X.copy()
        for col, encoder in self.encoders.items():
            output[col] = encoder.inverse_transform(output[col])
        return output
