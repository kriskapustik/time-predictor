# Импорт сторонних библиотек
from pydantic import BaseModel, Field


# Определение модели входных данных
class PredictionIn(BaseModel):
    region: str = Field(description='регион магазина, в котором собирался заказ, напр. "msk_region"')
    darkstore: bool = Field(description='флаг является ли магазин даркстором, напр. false')
    products_count: int = Field(description='общее кол-во уникальных позиций в корзине, напр. 5')
    quantity_no_weight_products: int = Field(description='кол-во штук всех невесовых товаров, напр. 5')
    weight_of_weight_products: float = Field(description='общий вес всех весовых товаров в корзине, напр. 3.7')
    marked_count: int = Field(description='кол-во уникальных позиций с маркировкой, напр. 1')
    weight_count: int = Field(description='кол-во уникальных весовых позиций, напр. 2')
    freeze_products: bool = Field(description='флаг присутствия товаров с заморозкой, напр. false')
    pay_on_packing: bool = Field(description='флаг оплаты на фасовке, напр. false')
    replacement_type: str = Field(description='категория типа замен, напр. "ask"')
    replacement_need_call: bool = Field(description='флаг включена ли звонящая сборка, напр. false')
    shop_group: str = Field(description='наименование группы магазинов, в которых собирался заказ')
    date_collecting: str = Field(description='Дата начала сборки, 2023-08-01T21:40:30.555Z')
    shop_timezone_offset: int = Field(description='Смещение времени таймзоны магазина в минутах, напр. 180')


# Определение модели выходных данных
class PredictionOut(BaseModel):
    prediction: int = Field(description='вывод результата предсказания в секундах, напр. 282')
