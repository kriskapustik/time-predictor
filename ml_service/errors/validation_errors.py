from fastapi import HTTPException
#from config import LOGGER

# Определение класса ошибок проверки данных
class ValidationError(HTTPException):
    def __init__(self, name: str, message="Input data validation error"):
        self.status_code = 400
        self.name = name
        self.message = message
        super().__init__(self.status_code, self.name + ": " + self.message)
        #LOGGER.error('ValidationError: %s ', self)


# Определение класса ошибок проверки данных
class InternalError(HTTPException):
    def __init__(self, name: str, message="Technical error"):
        self.status_code = 500
        self.name = name
        self.message = message
        super().__init__(self.status_code, self.name + ": " + self.message)
        #LOGGER.error('InternalError: %s ', self)
