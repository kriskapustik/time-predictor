import logging.config
import os
from logging.handlers import TimedRotatingFileHandler
import statsd

ENV = 'local'
logging.basicConfig()
LOGGER = logging.getLogger()
LOGGER.handlers.clear()

# create file handler which logs even debug messages
path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'logs', ENV + 'detail.log')
open(path, mode='a')
fh = TimedRotatingFileHandler(path, when='D', interval=1, backupCount=10)
fh.setLevel(logging.DEBUG)

# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.INFO)

formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')

formatter_full = logging.Formatter('%(asctime)s:%(name)s:%(funcName)s:%(lineno)d:%(levelname)s - %(message)s')

ch.setFormatter(formatter)
fh.setFormatter(formatter_full)

LOGGER.addHandler(ch)
LOGGER.addHandler(fh)
LOGGER.setLevel(logging.DEBUG)

prod = {'statsd': {'host': 'metrics_host', 'port': 8126, 'root_metric': 'services.ml.forecast.'+ENV}}

METRICS = statsd.StatsClient(prod['statsd']['host'], prod['statsd']['port'], prefix=prod['statsd']['root_metric'])