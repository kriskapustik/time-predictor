# Предсказатель времени сборки заказа

## Описание работы сервиса:
Выдача предсказания времени сборки заказа по запросу для сервиса *Picker*.
Расчетное время сборки конкретного заказа запрашивается в момент создания заказа в сервисе *Picker* и далее передается в сервис курьерки для вызова курьера.

## Требования:
Типы данных для обучения зафиксированы в файле *data_upload_requirements.yml*

### Запуск в виртуальном окружении
```bash
python -m venv venv
source venv/bin/activate
pip3 install --upgrade pip
pip3 install -r requirements.txt

export $(xarg < .env) (for Linux) или export $(cat .env | xargs -L 1) (for Mac os)
uvicorn main:app --reload
```

### Запуск в докере:
```bash
cd forecast_time_picker/
docker build --no-cache -t forecast_time_picker .
docker run -d --rm --name ftp -p 8000:8000 forecast_time_picker
```

### Как получить предсказание:
После старта сервиса отправить http-запрос на api по одному из вариантов
```bash
'localhost:8000/predict'
'http://127.0.0.1:8000/predict'
'http://0.0.0.0:8000/predict'
```


#### Пример запроса:

```bash
curl --location 'localhost:8000/predict' \
--header 'Content-Type: application/json' \
--data '{
    "region": "msk",
    "darkstore": false,
    "products_count": 5,
    "quantity_no_weight_products": 5,
    "weight_of_weight_products": 3.70,
    "marked_count": 1,
    "weight_count": 2,
    "freeze_products": false,
    "pay_on_packing": false,
    "replacement_type": "ask",
    "replacement_need_call": false,
    "shop_group": "f********",
    "date_collecting": "2023-08-01T21:40:30.555Z",
    "shop_timezone_offset": 180
}'
```

#### Пример ответа:
```bash
{"prediction":285}
```
Предсказание времени сборки заказа выдается в секундах





