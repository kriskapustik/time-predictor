from fastapi import FastAPI
#from errors import ValidationError, InternalError
from controllers import Predict
from data_types import PredictionIn, PredictionOut
import uvicorn

app = FastAPI()
# Определение маршрута для предсказания
@app.post('/predict/', response_model=PredictionOut)
async def predict(item: PredictionIn):
    """
     Запрос предсказания времени сборки
    """
    return Predict.predict_features(item)
